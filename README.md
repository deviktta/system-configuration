# deviktta's - System configuration

* [Debian 11 "bullseye" - System configuration](docs/debian-11-system-configuration.md)
* [Debian 11 "bullseye" - Software list](docs/debian-11-software-list.md)

* [Linux - Set mouse speed & sensitivity](docs/linux-set-mouse-speed-sensitivity.md)
* [Linux scripts - Hardware: Logitech G512 keyboard LED configuration](scripts/logitech-512.sh)
