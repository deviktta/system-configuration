# Linux: Set mouse speed & sensitivity

1. First, we need to determine our mouse device id.
    ```
    xinput list
    ```

2. Now, we set mouse acceleration to 0.
    ```
    # Set mouse acceleration to 0.
    xinput --set-prop <device_id> 'libinput Accel Speed' 0
   
    # Set X&Y scaling to 1, X&Y rotation and translation to 0 and 2D transformation to 0.
    xinput --set-prop <device_id> 'Coordinate Transformation Matrix' 1 0 0 0 1 0 0 0 1
    ```
   


