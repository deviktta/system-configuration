# Debian 11 - Software list

| Name              | Type                     | Method                                      | Method link                                      |
|-------------------|--------------------------|---------------------------------------------|--------------------------------------------------|
| **Balena Etcher** | Image files tool         | Website download - _.deb_ file              | https://etcher.balena.io/                        |
| **Dbeaver CE**    | Database tool            | Website download - _.deb_ file              | https://dbeaver.io/download/                     |
| **Dia**           | Diagram creation         | Apt                                         | http://dia-installer.de                          |
| **Filezilla**     | FTP tool                 | Apt                                         | https://filezilla-project.org                    |
| **Firefox ESR**   | Browser                  | Apt                                         | https://www.mozilla.org/en-US/firefox            |
| **Firefox DE**    | Browser                  | Website download - standalone app           | https://www.mozilla.org/en-US/firefox/developer/ |
| **Flameshot**     | Screenshot tool          | Apt                                         | https://flameshot.org                            |
| **Ghostwriter**   | Text editor for Markdown | Apt                                         | https://ghostwriter.kde.org/                     |
| Git Kraken        | Git GUI                  | Website download - _.deb_ file              | https://www.gitkraken.com/download               |
| Google Chrome     | Browser                  | Website download - _.deb_ file              | https://www.google.com/chrome/                   |
| **OpenRGP**       | Hardware utility         | Website download - _.deb_ file              | https://openrgb.org/releases.html               |
| Postman           | API client               | Website download - standalone app           | https://www.postman.com/downloads/               |
| Slack             | Communication            | Website download - _.deb_ file              | https://slack.com/downloads/linux                |
| Spotify           | Spotify client           | Website configuration - custom _apt_ source | https://www.spotify.com/es/download/linux/       |
| Screaming Frog    | Website crawler          | Website download - _.deb_ file              | https://www.screamingfrog.co.uk/seo-spider/      |
| **VLC**           | Video player             | Apt                                         | https://www.videolan.org/vlc                     |

_In bold, Open Source Software._


### Software specific-configuration

**Firefox _Extended Support Release_:**
* Settings:
  * Theme: Spacy Fox https://addons.mozilla.org/en-US/firefox/addon/spacy-fox
  * Toolbar:
    1. Downloads (always shown)
    2. Private browsing
    3. [AdBlocker Ultimate](https://addons.mozilla.org/en-US/firefox/addon/adblocker-ultimate)
    4. [Facebook container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container)
    5. [Bitwarden](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
    6. Firefox accounts
  * Preferences:
    * Home:
      * Homepage and new windows: blank page.
      * New tabs: blank page.
      * Firefox Home Content: uncheck all (empty home).
    * Privacy & Security:
      * Mode: Standard.
      * Check both 'tell websites not sell/share my data' & 'send website "Do Not Track"'.
      * Passwords: uncheck 'ask to save password'.
      * Autofill: uncheck 'save and fill payment methods', check 'Show alert about passwords for breached websites'.
      * History: remember history.
      * Permissions: uncheck 'block pop-up windows' and check 'warn about install add-ons'.
      * Firefox Data Collection and Use: uncheck all.
    * Sync: Bookmarks, Settings, Addons.
  * `about:config`:
    * browser.translations.automaticallyPopup: false

**Firefox _Developer Edition_:**
* Settings:
  * Theme: Dark Fox https://addons.mozilla.org/en-US/firefox/addon/dark-fox-18066
  * Toolbar:
    1. Downloads (always shown)
    2. Private browsing
    3. Developer tools
    4. [React Developer Tools](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/)
    5. [Dataslayer](https://addons.mozilla.org/en-US/firefox/addon/dataslayer-firefox)
    6. [Wappalyzer](https://addons.mozilla.org/en-US/firefox/addon/wappalyzer)
    7. [Bitwarden](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/)
    8. Firefox accounts
  * Preferences:
    * Home:
      * Homepage and new windows: blank page.
      * New tabs: blank page.
      * Firefox Home Content: uncheck all (empty home).
    * Privacy & Security:
      * Mode: Standard.
      * Check both 'tell websites not sell/share my data'.
      * Passwords: uncheck all 'ask to save password'.
      * Autofill: uncheck 'save and fill payment methods'.
      * History: remember history.
      * Permissions: uncheck 'block pop-up windows' and check 'warn about install add-ons'.
      * Firefox Data Collection and Use: uncheck all.
    * Sync: Bookmarks, Settings, Addons.
  * `about:config`:
    * browser.translations.automaticallyPopup: false
