# Debian 11 - System configuration

* Toolbar icons:
  * _APPS_
  * Terminator
  * Files
  * Text Editor
  * Firefox
  * Firefox Nightly
  * Google Chrome
  * Spotify
* Extensions:
  * Place status indicator
    * Removable drive menu
    * Windows list
      * Never group windows: on
      * Show on all monitors: on
    * System Monitor Next
      * Extension for GNOME to see current resources use: https://extensions.gnome.org/extension/3010/system-monitor-next/
      * Installation method: Firefox widget
    * Dash To Panel
      * Extension for GNOME to see current resources use: https://extensions.gnome.org/extension/1160/dash-to-panel/
      * Installation method: Firefox widget
      * Settings:
        * Position:
          * Display the main panel on: Primary monitor
          * Display panels on all monitors: off
          * Panel Intellihide: off
        * Style:
          * Running indicator position: Bottom
          * Running indicator style (Focused app): Dots
          * Running indicator style (Unfocused apps): Dots
        * Behavior:
          * Ungroup applications: off
          * Show windows previews on hover: on
        * Action:
          * Click action: Cycle windows + minimize
            * Shift+Click action: Launch new instance
            * Middle-Click action: Cycle through windows
            * Shift+Middle-Click action: Quit
* Power:
  * Power Mode: Balanced
  * Power saving options:
    * Dim Screen: off
    * Screen Blank: 10 mins
    * Automatic Power Saver: off
    * Automatic suspend: everything off
  * Power Button Behavior: Power Off
  * Show Battery Percentage: on
* Multitasking:
  * Hot Corner: off
  * Active Screen Edges: on
  * Fixed number of workspaces: 2
  * Workspaces on primary display only
* Privacy & Security:
  * Screen Lock
    * Blank Screen Delay: 10 minutes
    * Automatic Screen Lock Delay: Screen Turns Off
    * Lock Screen Notification: on
  * Locations Services: off
  * File History & Trash:
    * File History
      * File History: on
      * File History Duration: 30 days
    * Trash & Temporary Files
      * Automatically Delete Trash Content: on
      * Automatically Delete Temporary Files: on
      * Automatically Delete Period: 30 days
* Sharing:
  * File Sharing: off
  * Remote Desktop: off
  * Media Sharing: off
